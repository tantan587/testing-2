import fetch from 'isomorphic-fetch'

export default () => {
  fetch('/api/serverHello')
  .then((response) => response.json())
  .then((data) =>console.log(data))
}