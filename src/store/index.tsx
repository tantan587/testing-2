import {createStore, applyMiddleware, combineReducers, AnyAction} from 'redux';
import  {MakeStore} from 'next-redux-wrapper';
import thunk from 'redux-thunk'
import { systemReducer } from './system/reducer'
import { chatReducer } from './chat/reducer'

const rootReducer = combineReducers({
  system: systemReducer,
  chat: chatReducer,
  base: (state = {}, action: AnyAction): any => {
    switch (action.type) {
      default:
        return state
    }
  }
})

export type RootState = ReturnType<typeof rootReducer>

const clientLogger = (store : any) => (next : any) => (action:AnyAction) => {
  console.log(action)
    console.groupCollapsed('dispatching', action.type)
    console.log('prev state', store.getState())
    console.log('action', action)
    let result = next(action)
    console.log('next state', store.getState())
    console.groupEnd()
    return result
}

const middleware = () => [
  clientLogger,
  thunk
]

export const makeStore: MakeStore = (initialState: RootState) => {
  return applyMiddleware(...middleware())(createStore)(rootReducer, initialState)

    // if (module.hot) {
    //     module.hot.accept('./reducer', () => {
    //         console.log('Replacing reducer');
    //         store.replaceReducer(require('./reducer').default);
    //     });
    // }
};