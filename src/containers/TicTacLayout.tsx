  
import React, {FunctionComponent, ReactNode} from 'react';
import {connect} from 'react-redux';
import { RootState } from '../store';
import {SystemState} from '../store/system/types'
//import { RootState } from '../store';

interface IProps {
  sys : SystemState,
  children? : ReactNode
}

const TicTacLayout: FunctionComponent<IProps> = (props) => {
  console.log(props)
  return (
    <div className="layout">
        <div>Redux tick</div>
        <div>Redux toe</div>
        {props.children}
    </div>
)};

const mapStateToProps = (state: RootState) => {
  return {
    sys : state.system
  };
};


export default connect(mapStateToProps)(TicTacLayout);