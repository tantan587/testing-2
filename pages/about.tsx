import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ProTip from '../src/components/ProTip';
import Link from '../src/components/Link';
import Copyright from '../src/components/Copyright';
import { Button } from '@material-ui/core';
import getServerHello from '../src/store/hello/getServerHello'

export default function About() {
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          Next.js with TypeScript example
        </Typography>
        <Link href="/">Go to the main page</Link>
        <ProTip />
        <Copyright />
        <Button variant="contained" color="secondary" onClick={() => getServerHello()} >
          Get Result From Server in Console
        </Button>

      </Box>
    </Container>
  );
}
