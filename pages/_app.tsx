import React from 'react';
import Head from 'next/head';
//import { AppProps } from 'next/app';
import { ThemeProvider } from '@material-ui/core/styles';
import withRedux, {ReduxWrapperAppProps} from 'next-redux-wrapper';
import {makeStore} from '../src/store/index';
import {Provider} from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../styles/theme';
import TicTacLayout from '../src/containers/TicTacLayout'
//import State from '../src/state/reducers/reducer'
import '../styles/global.css'


const MyApp : React.FC<ReduxWrapperAppProps>  = (props: ReduxWrapperAppProps) => {
  const { Component, pageProps, store } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement!.removeChild(jssStyles);
    }
  }, []);
  console.log(store.getState())
  return (
    <React.Fragment>
      <Head>
        <title>My page</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Provider store={store}>
            <TicTacLayout>
                <Component {...pageProps} />
            </TicTacLayout>
        </Provider>
      </ThemeProvider>
    </React.Fragment>
  );
}

export default withRedux(makeStore, {debug:false})(MyApp)

//maybe for later
// class MyApp extends App<ReduxWrapperAppProps<State>> {
//   public static async getInitialProps({Component, ctx}: AppContext) {
//       // Keep in mind that this will be called twice on server, one for page and second for error page
//       await new Promise<any>(res => {
//           setTimeout(() => {
//               ctx.store.dispatch({type: 'TOE', payload: 'was set in _app'});
//               res();
//           }, 200);
//       });

//       return {
//           pageProps: {
//               // Call page-level getInitialProps
//               ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
//               // Some custom thing for all pages
//               pathname: ctx.pathname,
//           },
//       };
//   }
