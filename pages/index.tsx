import React, {ElementType} from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ProTip from '../src/components/ProTip';
import Link from '../src/components/Link';
import Copyright from '../src/components/Copyright';
import Button from '@material-ui/core/Button'
import {Layout} from '../src/containers/Layout'


export default function Index() {

  return (
    <Layout home>
      <Container maxWidth="sm">
        <Box my={4}>
          <Typography variant="h4" component="h1" gutterBottom>
            Next.js with TypeScript example
          </Typography>
          <Link href="/about" color="secondary">
            Go to the about page
          </Link>
          <br/>
          <Button variant="contained" color="primary" component={Link as ElementType} naked href="/counter">
            Go to the couter page
          </Button> 
          <ProTip />
          <Copyright />
        </Box>
      </Container>
    </Layout>
  );
}
